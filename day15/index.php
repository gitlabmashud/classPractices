<html>
<head>
	<title>Form</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<fieldset>
					<legend>What type of Movies Do You Like?</legend>

					<form action="" method="post">

						<label for="movie1">
							<input type="checkbox" name="movie[]" id="movie1" value="Comedy">
							Comedy
						</label>

						<br><br><br>

						<label for="movie2">
							
							<input type="checkbox" name="movie[]" id="movie2" value="Action">
							Action
						</label>

						<br><br><br>

						<label for="movie3">
							
							<input type="checkbox" name="movie[]" id="movie3" value="Science Fiction">
							Science Fiction
						</label>

						<br><br><br>

						<label for="movie4">
							
							<input type="checkbox" name="movie[]" id="movie4" value="Romance">
							Romance
						</label>

						<br><br><br>

						<label for="movie5">
							
							<input type="checkbox" name="movie[]" id="movie5" value="Animation">
							Animation
						</label>

						<br><br><br>



						<button type="submit">Selected Movies</button>
						

					</form>

				</fieldset>
			</div>
		</div>
	</div>

	<?php
		if(!empty($_POST['movie'])) {
		    foreach($_POST['movie'] as $check) {
		            echo $check;
		    }
		}
	?>
</body>
</html>